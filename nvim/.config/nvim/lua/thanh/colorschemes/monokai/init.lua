local status_ok, monokai = pcall(require, 'monokai')
if not status_ok then
	return
end

local colors = monokai.soda

monokai.setup{
	palette = colors,
	custom_hlgroups = {
		Visual = {
			bg = '#436BD1'
		},
		LineNr = {
			fg = colors.orange
		},
		TSFunction = {
			fg = colors.green,
		},
		TSFuncBuiltin = {
			fg = colors.green,
			style = 'italic'
		},
		TSConstructor = {
			fg = colors.green,
			style = 'bold'
		},
		TSFuncMacro = {
			fg = colors.green,
			style = 'bold',
		},
		TSVariable = {
			fg = colors.aqua
		},
		TSVariableBuiltin = {
			fg = colors.aqua,
			style = 'italic'
		},
		TSKeywordFunction = {
			fg = colors.green,
			style = 'italic',
		},
		TSKeywordReturn = {
			fg = colors.orange,
			style = 'italic',
		},
		TSType = {
			fg = colors.purple,
		},
		TSField = {
			fg = colors.purple,
		},
		TSProperty = {
			fg = colors.purple,
		},
		TSNumber = {
			fg = colors.green,
		},
		TSConstant = {
			fg = colors.yellow,
		},
		TSConstBuiltin = {
			fg = colors.yellow,
			style = 'italic'
		},
		TSConstMacro = {
			fg = colors.yellow,
			style = 'bold'
		},
		LspReferenceText = {
			bg = colors.base5
		},
		LspReferenceWrite = {
			bg = colors.base5
		},
		LspReferenceRead = {
			bg = colors.base5
		},
	}
}
