local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
	PACKER_BOOTSTRAP = fn.system {
		"git",
		"clone",
		"--depth",
		"1",
		"https://github.com/wbthomason/packer.nvim",
		install_path,
	}
	print "Installing packer, restart"
	vim.cmd [[packadd packer.nvim]]
end

vim.cmd [[
	augroup packer_user_config
		autocmd!
		autocmd BufWritePost plugins.lua source <afile> | PackerSync
	augroup end
]]

local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

return packer.startup(function(use)

	-- Packer
	use 'wbthomason/packer.nvim'

	-- Misc.
	use 'nvim-lua/popup.nvim'
	use 'nvim-lua/plenary.nvim'
	use 'onsails/lspkind-nvim'
	use 'norcalli/nvim-colorizer.lua'
	use 'windwp/nvim-autopairs'
	use 'windwp/nvim-ts-autotag'
	use 'danymat/neogen'
	use 'kyazdani42/nvim-web-devicons'
	use 'kyazdani42/nvim-tree.lua'
	use 'NTBBloodbath/galaxyline.nvim'

	-- Completion
	use 'hrsh7th/nvim-cmp'
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-path'
	use 'hrsh7th/cmp-cmdline'
	use 'saadparwaiz1/cmp_luasnip'
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-nvim-lua'
	use 'rcarriga/cmp-dap'

	-- LSP
	use 'neovim/nvim-lspconfig'
	use 'williamboman/nvim-lsp-installer'
	use 'RRethy/vim-illuminate'
	use 'tamago324/nlsp-settings.nvim'

	-- Snippets
	use 'L3MON4D3/LuaSnip'
	use 'rafamadriz/friendly-snippets'

	-- Colors
	use 'tanvirtin/monokai.nvim'
	use 'navarasu/onedark.nvim'

	-- Telescope
	use 'nvim-telescope/telescope.nvim'

	--Treesitter
	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate'
	}

	-- Git
	use 'lewis6991/gitsigns.nvim'

	-- Debugging
	use 'mfussenegger/nvim-dap'
	use 'sakhnik/nvim-gdb'
	use 'rcarriga/nvim-dap-ui'
	use 'theHamsta/nvim-dap-virtual-text'

	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end

end)
