local status_ok, autotags = pcall(require, "nvim-ts-autotag")
if not status_ok then
	return
end
local ts_status_ok, tsconf = pcall(require, "nvim-treesitter.configs")
if not ts_status_ok then
	return
end

tsconf.setup{
	autotag = {
		enable = true,
		filetypes = {
			'html', 'javascript', 'typescript', 'javascriptreact', 'typescriptreact', 'svelte', 'vue', 'tsx', 'jsx', 'rescript', 'latte',
			'xml',
			'php',
			'markdown',
			'glimmer','handlebars','hbs'
		}
	}
}
