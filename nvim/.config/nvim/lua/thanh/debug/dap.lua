local dap_status_ok, dap = pcall(require, "dap")
if not dap_status_ok then
	return
end

vim.fn.sign_define('DapBreakpoint', {text='', texthl='DiagnosticSignError', linehl='', numhl=''})
vim.fn.sign_define('DapBreakpointCondition', {text='', texthl='DiagnosticSignHint', linehl='', numhl=''})
vim.fn.sign_define('`DapStopped`', {text='', texthl='diffAdded', linehl='', numhl=''})

dap.adapters.php = {
  type = 'executable',
  command = 'node',
  args = { '/home/thanh/Data/vscode-php-debug/out/phpDebug.js' }
}

dap.configurations.php = {
  {
    type = 'php',
    request = 'launch',
    name = 'Listen for Xdebug',
    port = 9003,
	pathMappings = {
		["/var/www/html"] = "${workspaceFolder}"
	}
  }
}
