local status_ok, _ = pcall(require, 'lspconfig')
if not status_ok then
	return
end

require("thanh.lsp.lsp-installer")
require("thanh.lsp.handlers").setup()
require("thanh.lsp.nlspsettings")
