local dap_status_ok, dap = pcall(require, "dap")
if not dap_status_ok then
	return
end

local dap_ui_status_ok, dapui = pcall(require, "dapui")
if not dap_ui_status_ok then
	print("dapui not found")
	return
end

dapui.setup {
  icons = { expanded = "▾", collapsed = "▸" },
  mappings = {
    -- Use a table to apply multiple mappings
    expand = { "<CR>", "<2-LeftMouse>", "l" },
    open = "o",
    remove = "d",
    edit = "e",
    repl = "r",
    toggle = "t",
  },
  layouts = { 
	{
		elements = { "scopes", "stacks", "watches"
			--{
			--	id = "scopes",
			--	size = 0.5, -- Can be float or integer > 1
			--},
			--{ 
			--	id = "stacks", size = 0.25 },
			--{ 	
			--	id = "watches", size = 0.25
			--},
		},
		size = 50,
		position = "right", -- Can be "left", "right", "top", "bottom"
	},
	{
		elements = { "repl" },
		size = 20,
		position = "bottom", -- Can be "left", "right", "top", "bottom"
	},
  },
}

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end

dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end

dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end
