local status_ok, dap_virt_text = pcall(require, 'nvim-dap-virtual-text')
if not status_ok then
	return
end

dap_virt_text.setup{
	enabled = true,
	enabled_commands = true,
	all_references = true,
	only_first_definition = true,
	highlight_new_as_changed = false,
	highlight_changed_variables = false
}
