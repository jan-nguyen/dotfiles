local opts = { noremap = true }
local keymap = vim.api.nvim_set_keymap

vim.g.mapleader = " "

-- Normal

-- Misc
keymap("n", "gf", ":vsp <cfile><cr>", opts)
keymap("n", "Y", 'y$', opts)
keymap("n", "n", "nzzzv", opts)
keymap("n", "N", "Nzzzv", opts)
keymap("n", "J", 'mzJ`z', opts)

-- Move text
keymap("n", "<leader>j", ":m .+1<CR>==", opts)
keymap("n", "<leader>k", ":m .-2<CR>==", opts)

-- Better split navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- QuickFix lists
-- local
keymap("n", "<leader>lo", ":lopen<CR>", opts)
keymap("n", "<leader>ln", ":lnext<CR>", opts)
keymap("n", "<leader>lp", ":lprev<CR>", opts)

-- global
keymap("n", "<leader>qo", ":copen<CR>", opts)
keymap("n", "<leader>qc", ":cclose<CR>", opts)
keymap("n", "<leader>qn", ":cnext<CR>", opts)
keymap("n", "<leader>qp", ":cprev<CR>", opts)

-- Telescope
keymap("n", "<leader>pf", ":lua require'telescope.builtin'.find_files()<cr>", opts)
keymap("n", "<leader>ps", ":lua require'telescope.builtin'.live_grep()<cr>", opts)
keymap("n", "<leader>af", ":lua require('neogen').generate({type = 'func'})<cr>", opts)
keymap("n", "<leader>ac", ":lua require('neogen').generate({type = 'class'})<cr>", opts)

-- Debugging
keymap("n", "<leader>dbp", ":lua require('dap').toggle_breakpoint()<cr>", opts)
keymap("n", "<leader>dbc", ":lua require('dap').toggle_breakpoint(vim.fn.input('Breakpoint condition: '))<cr>", opts)
keymap("n", "<leader>dco", ":lua require('dap').continue()<cr>", opts)
keymap("n", "<leader>dsn", ":lua require('dap').step_over()<cr>", opts)
keymap("n", "<leader>dsi", ":lua require('dap').step_into()<cr>", opts)
keymap("n", "<leader>dso", ":lua require('dap').step_out()<cr>", opts)

-- Nvim Tree
keymap("n", "<leader>e", ":NvimTreeToggle<cr>", opts)

-- Visual

keymap("v", 'J', ":m \'>+1<CR>gv=gv", opts)
keymap("v", 'K', ":m \'<-2<CR>gv=gv", opts)
keymap("v", '>', '>gv', opts)
keymap('v', '<', '<gv', opts)
keymap('v', '<leader>p', '"_dP', opts)
keymap("v", '<C-A>', '<C-A>gv', opts)
keymap("v", '<C-X>', '<C-X>gv', opts)
