local status_ok, gl = pcall(require, 'galaxyline')
if not status_ok then
	return
end
local gls = gl.section
gl.short_line_list = {"NvimTree"}

local colors = {
  bg = "#2e323c",
  yellow = "#dfd561",
  cyan = "#78dce8",
  darkblue = "#081633",
  green = "#97e023",
  orange = "#fa8419",
  purple = "#9c64fe",
  magenta = "#d16d9e",
  grey = "#72696a",
  blue = "#78dce8",
  red = "#f3005f",
}

local buffer_not_empty = function()
  if vim.fn.empty(vim.fn.expand("%:t")) ~= 1 then
    return true
  end
  return false
end

gls.left[1] = {
  ViMode = {
    provider = function()
      local alias = {
        n = "NORMAL",
        i = "INSERT",
        c = "COMMAND",
        v = "VISUAL",
        V = "VISUAL LINE",
        [""] = "VISUAL BLOCK",
      }
      return alias[vim.fn.mode()]
    end,
    separator = " ",
    separator_highlight = {
      colors.purple,
      function()
        if not buffer_not_empty() then
          return colors.purple
        end
        return colors.darkblue
      end,
    },
    highlight = { colors.darkblue, colors.purple, "bold" },
  },
}
gls.left[2] = {
  FileIcon = {
    provider = "FileIcon",
    condition = buffer_not_empty,
    highlight = { require("galaxyline.providers.fileinfo").get_file_icon_color, colors.darkblue },
  },
}
gls.left[3] = {
  FileName = {
    provider = {"FileName"},
    condition = buffer_not_empty,
    separator = "",
    highlight = { colors.magenta, colors.darkblue },
	separator_highlight = { colors.darkblue , colors.grey }
  },
}

gls.left[4] = {
  GitIcon = {
    provider = function()
      return "  "
    end,
    condition = buffer_not_empty,
    highlight = { colors.orange, colors.grey },
  },
}
gls.left[5] = {
  GitBranch = {
    provider = "GitBranch",
    separator = " ",
    condition = buffer_not_empty,
    highlight = { colors.orange, colors.grey },
	separator_highlight = { colors.grey, colors.darkblue }
  },
}

local checkwidth = function()
  local squeeze_width = vim.fn.winwidth(0) / 2
  if squeeze_width > 40 then
    return true
  end
  return false
end

gls.left[6] = {
  DiffAdd = {
    provider = "DiffAdd",
    condition = checkwidth,
    icon = " ",
    highlight = { colors.magenta, colors.darkblue },
  },
}
gls.left[7] = {
  DiffModified = {
    provider = "DiffModified",
    condition = checkwidth,
    icon = " ",
    highlight = { colors.magenta, colors.darkblue },
  },
}
gls.left[8] = {
  DiffRemove = {
    provider = "DiffRemove",
    condition = checkwidth,
    icon = " ",
    highlight = { colors.magenta, colors.darkblue },
  },
}
gls.left[9] = {
  LeftEnd = {
    provider = function()
      return ""
    end,
    separator = "",
    separator_highlight = { colors.darkblue, colors.grey },
    highlight = { colors.darkblue, colors.darkblue },
  },
}
gls.left[10] = {
  DiagnosticError = {
    provider = "DiagnosticError",
    icon = "  ",
    highlight = { colors.red, colors.grey },
  },
}
gls.left[11] = {
  Space = {
    provider = function()
      return " "
    end,
	highlight = {colors.grey, colors.grey}
  },
}
gls.left[12] = {
  DiagnosticWarn = {
    provider = "DiagnosticWarn",
    icon = "  ",
    highlight = { colors.blue, colors.grey },
  },
}
gls.right[1] = {
  LineInfo = {
    provider = "LineColumn",
    separator = "",
    separator_highlight = { colors.darkblue, colors.grey },
    highlight = { colors.magenta, colors.darkblue },
  },
}
gls.right[2] = {
  PerCent = {
    provider = "LinePercent",
    separator = "",
    separator_highlight = { colors.purple, colors.darkblue },
    highlight = { colors.darkblue, colors.purple },
  },
}

gls.short_line_left[1] = {
  BufferType = {
    provider = "FileName",
    separator = "",
    separator_highlight = { colors.purple, colors.bg },
    highlight = { colors.darkblue, colors.purple },
  },
}

gls.short_line_right[1] = {
  BufferIcon = {
    provider = "BufferIcon",
    separator = "",
    separator_highlight = { colors.purple, colors.bg },
    highlight = { colors.darkblue, colors.purple },
  },
}
